//
//  SwiftString.swift
//  
//
//  Some simple string utilities for Swift
//
//

import Foundation


// Converts a number to a text description of the number,
// handling anything from zero to 999.
func NumberWord(num: Int) -> String {
  switch num {
  case 0: return "zero"
  case 1: return "one"
  case 2: return "two"
  case 3: return "three"
  case 4: return "four"
  case 5: return "five"
  case 6: return "six"
  case 7: return "seven"
  case 8: return "eight"
  case 9: return "nine"
  case 10: return "ten"
  case 11: return "eleven"
  case 12: return "twelve"
  case 13: return "thirteen"
  case 14, 16, 17, 18, 19: return NumberWord(num-10) + "teen"
  case 15: return "fifteen"
  case 20: return "twenty"
  case 30: return "thirty"
  case 40: return "forty"
  case 50: return "fifty"
  case 80: return "eighty"
  case 60, 70, 90: return NumberWord(num / 10) + "ty"
  case 21...99: return NumberWord((num / 10) * 10) + "-" + NumberWord(num % 10)
  case 100, 200, 300, 400, 500, 600, 700, 800, 900: return NumberWord(num / 100) + " hundred"
  case 101...999: return NumberWord((num / 100) * 100) + " and " + NumberWord(num % 100)
  default: return "an awful lot"
  }
}


// Smashes case to title case -- first letter capped. Doesn't currently
// handle multiple word strings though (for future implementation).
func TitleCase(s: String) -> String {
  var firstChar = s.substringToIndex(advance(s.startIndex,1)).uppercaseString
  var rest = s.substringFromIndex(advance(s.startIndex,1)).lowercaseString
  return firstChar + rest
}

